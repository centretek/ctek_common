<?php

namespace Drupal\ctek_common\Logger;

use Drupal\Component\Serialization\Json;

class BatchLog implements \Iterator {

  protected $file;

  public function __construct(\SplFileObject $file) {
    $this->file = $file;
  }

  public function valid(): bool {
    return $this->file->valid();
  }

  public function key(): mixed {
    return $this->file->key();
  }

  public function current(): mixed {
    return Json::decode($this->file->current());
  }

  public function next(): void {
    $this->file->next();
  }

  public function rewind(): void {
    $this->file->rewind();
  }

}
